<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all()->sortByDesc('created_at');
        return response(PostResource::collection($posts), 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response([
                "message" => implode(' ', $validator->errors()->all()),
            ], 400);
        }
        $post = Post::create([
            'user_id' => $request->user()->id,
            'content' => $request->content,
        ]);
        return response([], 201);
        //return response(new PostResource($post), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return response(new PostResource($post), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if (!$post) {
            return response(['message' => "Post id {$id}] does not exist!"] ,201);
        }
        $validator = Validator::make($request->all(), [ 
            'content' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response([
                "message" => implode(' ', $validator->errors()->all()),
            ], 400);
        }
        $post->update($validator->validated());
        return response(new PostResource($post), 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return response([], 201);
    }
}
