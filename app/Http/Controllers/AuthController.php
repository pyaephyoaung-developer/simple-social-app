<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserLoginResource;

class AuthController extends Controller
{

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response([
                "message" => implode(' ', $validator->errors()->all()),
            ], 400);
        }
        
        $field = $validator->validated();

        $user = User::where('email', $field['username'])->first();
        if (!$user || !Hash::check($field['password'], $user->password)) {
            $response = [
                'message' => 'Email or password does not match!'
            ];
            return response($response, 401);
        }
        return response(new UserLoginResource($user), 201);
    }

    public function logout(Request $request) {
        $user = auth()->user();
        if ($user) {
            $user->tokens()->delete();
        }
        return response([], 201);
    }

}
