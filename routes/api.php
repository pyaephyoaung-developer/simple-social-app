<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::resource('/posts', PostController::class);
    //post all
    //Route::get('/posts', [PostController::class, 'index']);
    // //post create
    // Route::post('/posts', [PostController::class, 'store']);
    // //post delete
    // Route::delete('/posts', [PostController::class, 'destroy']);

    //logout
    Route::post('/users/logout', [AuthController::class, 'logout']);

    //
    Route::get('/users/{id?}', [UserController::class, 'show']);

});

//create new user
Route::post('/users', [UserController::class, 'store']);

//login
Route::post('/users/login', [AuthController::class, 'login'])->name('login');

// Route::get('/posts', [PostController::class, 'index']);
// Route::post('/posts', [PostController::class, 'store']);

